# Conrod-Prompt [![Crates.io](https://img.shields.io/crates/v/conrod_prompt.svg)](https://crates.io/crates/conrod_prompt) [![docs.rs](https://docs.rs/conrod_prompt/badge.svg)](https://docs.rs/conrod_prompt/)

A (very) simple library allowing the creation of prompt widgets in Conrod.

## License

Licensed under either:

 * Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.
