//! # Prompt widgets for conrod
//! These widgets are used to create prompts for the user to interact with on-screen.
//! The library is organized as a generic `Prompt` widget which has an internal widget
//! which accepts user interaction.
//!
//! Some common user interaction cases are implemented already:
//! - `Ack`: a prompt for when you want the user to see a message and acknowledge it
//! - `YesNo`: a prompt for when you want the user to see a message and indicate whether or not
//! they accept it

#[macro_use]
extern crate conrod_derive;

pub use ack::Ack;
pub use yesno::YesNo;

use conrod_core::{
    self, builder_method, builder_methods,
    color::{Color, Colorable},
    position, text,
    widget::{self, Widget},
    widget_ids, Borderable, FontSize, Labelable, Positionable, Scalar, Sizeable,
};

/// A general widget for displaying a floating prompt to the user.
/// The prompt displays some message and offers the user an opportunity
/// to respond to the message via a contained child widget, which
/// accepts some form of input.
#[derive(Clone, Debug, WidgetCommon)]
pub struct Prompt<'a, I> {
    #[conrod(common_builder)]
    common: widget::CommonBuilder,
    text: &'a str,
    // Input widget wrapped by the prompt.
    input: I,
    style: Style,
}

impl<'a, I> Prompt<'a, I> {
    /// Construct a new Prompt with the given prompt message.
    pub fn new(text: &'a str, input: I) -> Self {
        Prompt {
            common: widget::CommonBuilder::default(),
            text,
            input,
            style: Style::default(),
        }
    }

    /// Construct a new Prompt with the given message and style.
    pub fn with_style(text: &'a str, input: I, style: Style) -> Self {
        Prompt {
            common: widget::CommonBuilder::default(),
            text,
            input,
            style,
        }
    }

    pub fn label_font_id(mut self, font_id: text::font::Id) -> Self {
        self.style.label_font_id = Some(Some(font_id));
        self
    }

    /// A builder method that lifts the input widget through the given build function.
    /// Used as a slight ergonomic improvement for cases where the input
    /// widget is being styled and so forth.
    pub fn and_input<F>(mut self, build: F) -> Self
    where
        F: FnOnce(I) -> I,
    {
        self.input = build(self.input);
        self
    }

    /// A method that runs the given build function on the input widget.
    /// Used as a slight ergonomic improvement for cases where the input
    /// widget is being styled and so forth.
    ///
    /// If `maybe` is `Some(t)`, `build(self.input, t)` is evaluated and returned.
    /// If `maybe` is `None`, `self` is returned.
    pub fn and_then_input<F, T>(mut self, build: F, maybe: Option<T>) -> Self
    where
        F: FnOnce(I, T) -> I,
    {
        if let Some(v) = maybe {
            self.input = build(self.input, v);
        }
        self
    }
}

impl<'a> Prompt<'a, ack::Ack<'a>> {
    /// Return a new prompt which awaits user acknowledgment.
    pub fn ack(text: &'a str) -> Self {
        Prompt::new(text, ack::Ack::new())
    }
}

impl<'a> Prompt<'a, yesno::YesNo<'a>> {
    /// Return a new prompt which awaits a yes or no answer from the user.
    pub fn yes_no(text: &'a str) -> Self {
        Prompt::new(text, yesno::YesNo::new())
    }
}

impl<'a> Prompt<'a, widget::TextBox<'a>> {
    /// Return a new prompt which awaits a text response from the user.
    pub fn text_box(text: &'a str, box_text: &'a str) -> Self {
        Prompt::new(text, widget::TextBox::new(box_text))
    }
}

impl<'a> Prompt<'a, text_confirm::TextConfirm<'a>> {
    pub fn text_confirm(text: &'a str, box_text: &'a str) -> Self {
        Prompt::new(text, text_confirm::TextConfirm::new(box_text))
    }
}

#[derive(Copy, Clone, Debug, Default, PartialEq, WidgetStyle)]
pub struct Style {
    #[conrod(default = "theme.shape_color")]
    pub color: Option<Color>,
    #[conrod(default = "theme.border_width")]
    pub border: Option<Scalar>,
    #[conrod(default = "theme.border_color")]
    pub border_color: Option<Color>,
    #[conrod(default = "theme.label_color")]
    pub label_color: Option<Color>,
    #[conrod(default = "5.0")]
    pub padding: Option<Scalar>,
    #[conrod(default = "theme.font_size_medium")]
    pub label_font_size: Option<FontSize>,
    #[conrod(default = "theme.font_id")]
    pub label_font_id: Option<Option<text::font::Id>>,
    #[conrod(default = "text::Justify::Left")]
    pub label_justify: Option<text::Justify>,
    #[conrod(default = "position::Relative::Align(position::Align::Middle)")]
    pub label_x: Option<position::Relative>,
    #[conrod(default = "position::Relative::Align(position::Align::Middle)")]
    pub label_y: Option<position::Relative>,
}

widget_ids! {
    pub struct Ids {
        container,
        message,
        input,
    }
}

impl<'a, I> Widget for Prompt<'a, I>
where
    I: Widget,
{
    type State = Ids;
    type Style = Style;
    type Event = I::Event;

    fn init_state(&self, id_gen: widget::id::Generator) -> Self::State {
        Ids::new(id_gen)
    }

    fn style(&self) -> Self::Style {
        self.style
    }

    fn update(self, args: widget::UpdateArgs<Self>) -> Self::Event {
        let widget::UpdateArgs {
            id,
            state,
            style,
            rect,
            ui,
            ..
        } = args;

        // create the containing canvas
        let dim = rect.dim();
        let color = style.color(&ui.theme);
        let border_color = style.border_color(&ui.theme);
        let border = style.border(&ui.theme);
        widget::Canvas::new()
            .wh(dim)
            .middle_of(id)
            .graphics_for(id)
            .color(color)
            .border(border)
            .border_color(border_color)
            .floating(true) // make the container float
            .set(state.container, ui);

        let label_color = style.label_color(&ui.theme);
        let font_size = style.label_font_size(&ui.theme);
        let font_id = style
            .label_font_id(&ui.theme)
            .or_else(|| ui.fonts.ids().next());
        let msg_padding = style.padding(&ui.theme);
        let msg_widget = widget::Text::new(self.text)
            .and_then(font_id, widget::Text::font_id)
            .mid_top_of(state.container)
            .padded_w_of(state.container, msg_padding)
            .font_size(font_size)
            .graphics_for(id)
            .color(label_color);
        msg_widget.set(state.message, ui);

        // TODO: any styling on the child?
        self.input
            .parent(state.container)
            .mid_bottom_with_margin_on(state.container, msg_padding)
            .set(state.input, ui)
    }
}

impl<'a, I> Labelable<'a> for Prompt<'a, I> {
    builder_methods! {
        label { text = &'a str }
        label_color { style.label_color = Some(Color) }
        label_font_size { style.label_font_size = Some(FontSize) }
    }
}

impl<'a, I> Colorable for Prompt<'a, I> {
    builder_method!(color { style.color = Some(Color) });
}

impl<'a, I> Borderable for Prompt<'a, I> {
    builder_methods! {
        border { style.border = Some(Scalar) }
        border_color { style.border_color = Some(Color) }
    }
}

/// The `Ack` widget and related items.
pub mod ack {
    use super::*;
    use widget::button::Style;

    widget_ids! {
        pub struct Ids {
            button,
        }
    }

    /// Simply request that the user acknowledge the message.
    /// This is a wrapper around a single Button with the default text "OK".
    #[derive(Default, Debug, WidgetCommon)]
    pub struct Ack<'a> {
        #[conrod(common_builder)]
        common: widget::CommonBuilder,
        label: &'a str,
        style: Style,
    }

    impl<'a> Ack<'a> {
        pub fn new() -> Self {
            Ack {
                common: widget::CommonBuilder::default(),
                label: "OK",
                style: Style::default(),
            }
        }

        pub fn label_font_id(mut self, font_id: text::font::Id) -> Self {
            self.style.label_font_id = Some(Some(font_id));
            self
        }
    }

    impl<'a> Widget for Ack<'a> {
        type State = Ids;
        type Style = Style;
        type Event = Option<()>;

        fn init_state(&self, id_gen: widget::id::Generator) -> Self::State {
            Ids::new(id_gen)
        }

        fn style(&self) -> Self::Style {
            self.style
        }

        fn update(self, args: widget::UpdateArgs<Self>) -> Self::Event {
            let widget::UpdateArgs {
                id,
                state,
                style,
                rect,
                ui,
                ..
            } = args;

            let dim = rect.dim();
            let label_color = style.label_color(&ui.theme);
            let font_size = style.label_font_size(&ui.theme);
            let font_id = style
                .label_font_id(&ui.theme)
                .or_else(|| ui.fonts.ids().next());
            let times_clicked = widget::Button::new()
                .enabled(true)
                .label(self.label)
                .and_then(font_id, widget::Button::label_font_id)
                .label_color(label_color)
                .label_font_size(font_size)
                .middle_of(id)
                .wh(dim)
                .set(state.button, ui);

            if times_clicked.was_clicked() {
                Some(())
            } else {
                None
            }
        }
    }

    impl<'a> Labelable<'a> for Ack<'a> {
        builder_methods! {
            label { label = &'a str }
            label_color { style.label_color = Some(Color) }
            label_font_size { style.label_font_size = Some(FontSize) }
        }
    }
}

/// The YesNo widget and related items.
pub mod yesno {
    use super::*;
    /// Request that the user respond with yes or no.
    /// This is a wrapper around two side-by-side buttons with the default labels "Yes" and "No".
    #[derive(Default, Debug, WidgetCommon)]
    pub struct YesNo<'a> {
        #[conrod(common_builder)]
        common: widget::CommonBuilder,
        yes_label: &'a str,
        no_label: &'a str,
        style: Style,
    }

    #[derive(Copy, Clone, Debug, Default, PartialEq, WidgetStyle)]
    pub struct Style {
        pub yes: widget::button::Style,
        pub no: widget::button::Style,
    }

    impl<'a> YesNo<'a> {
        pub fn new() -> Self {
            YesNo {
                common: widget::CommonBuilder::default(),
                yes_label: "Yes",
                no_label: "No",
                style: Style::default(),
            }
        }

        pub fn yes_label_font_id(mut self, font_id: text::font::Id) -> Self {
            self.style.yes.label_font_id = Some(Some(font_id));
            self
        }

        pub fn no_label_font_id(mut self, font_id: text::font::Id) -> Self {
            self.style.no.label_font_id = Some(Some(font_id));
            self
        }

        builder_methods! {
            pub yes_label { yes_label = &'a str }
            pub yes_label_color { style.yes.label_color = Some(Color) }
            pub yes_label_font_size { style.yes.label_font_size = Some(FontSize) }
            pub no_label { no_label = &'a str }
            pub no_label_color { style.no.label_color = Some(Color) }
            pub no_label_font_size { style.no.label_font_size = Some(FontSize) }
        }
    }

    widget_ids! {
        pub struct Ids {
            canvas,
            left,
            right,
            yes,
            no,
        }
    }

    impl<'a> Widget for YesNo<'a> {
        type State = Ids;
        type Style = Style;
        type Event = Option<bool>;

        fn init_state(&self, id_gen: widget::id::Generator) -> Self::State {
            Ids::new(id_gen)
        }

        fn style(&self) -> Self::Style {
            self.style
        }

        fn update(self, args: widget::UpdateArgs<Self>) -> Self::Event {
            let widget::UpdateArgs {
                id,
                state,
                style,
                rect,
                ui,
                ..
            } = args;

            let dim = rect.dim();
            widget::Canvas::new()
                .middle_of(id)
                .graphics_for(id)
                .wh(dim)
                .flow_right(&[
                    (state.left, widget::Canvas::new()),
                    (state.right, widget::Canvas::new()),
                ])
                .set(state.canvas, ui);

            let yes_label_color = style.yes.label_color(&ui.theme);
            let yes_font_size = style.yes.label_font_size(&ui.theme);
            let yes_font_id = style
                .yes
                .label_font_id(&ui.theme)
                .or_else(|| ui.fonts.ids().next());
            let yes_label_x = style.yes.label_x(&ui.theme);
            let yes_label_y = style.yes.label_y(&ui.theme);
            let yes = widget::Button::new()
                .enabled(true)
                .label(self.yes_label)
                .and_then(yes_font_id, widget::Button::label_font_id)
                .label_color(yes_label_color)
                .label_font_size(yes_font_size)
                .label_x(yes_label_x)
                .label_y(yes_label_y)
                .middle_of(state.left)
                .wh_of(state.left)
                .set(state.yes, ui);

            let no_label_color = style.no.label_color(&ui.theme);
            let no_font_size = style.no.label_font_size(&ui.theme);
            let no_font_id = style
                .no
                .label_font_id(&ui.theme)
                .or_else(|| ui.fonts.ids().next());
            let no_label_x = style.no.label_x(&ui.theme);
            let no_label_y = style.no.label_y(&ui.theme);
            let no = widget::Button::new()
                .enabled(true)
                .label(self.no_label)
                .and_then(no_font_id, widget::Button::label_font_id)
                .label_color(no_label_color)
                .label_font_size(no_font_size)
                .label_x(no_label_x)
                .label_y(no_label_y)
                .middle_of(state.right)
                .wh_of(state.right)
                .set(state.no, ui);

            if yes.was_clicked() {
                Some(true)
            } else if no.was_clicked() {
                Some(false)
            } else {
                None
            }
        }
    }
}

pub mod text_confirm {
    use super::*;
    use widget::text_box;
    /// Request the user type some text, allowing them to cancel the prompt or confirm.
    /// This is a wrapper around a TextBox, plus
    /// two side-by-side buttons with the default labels "Cancel" and "OK".
    #[derive(Debug, WidgetCommon)]
    pub struct TextConfirm<'a> {
        #[conrod(common_builder)]
        common: widget::CommonBuilder,
        ok_label: &'a str,
        cancel_label: &'a str,
        text_box_text: &'a str,
        style: Style,
    }

    #[derive(Copy, Clone, Debug, Default, PartialEq, WidgetStyle)]
    pub struct Style {
        pub ok: widget::button::Style,
        pub cancel: widget::button::Style,
        pub text_box: widget::text_box::Style,
    }

    impl<'a> TextConfirm<'a> {
        pub fn new(text_box_text: &'a str) -> Self {
            TextConfirm {
                common: widget::CommonBuilder::default(),
                ok_label: "OK",
                cancel_label: "Cancel",
                text_box_text,
                style: Style::default(),
            }
        }

        pub fn ok_label_font_id(mut self, font_id: text::font::Id) -> Self {
            self.style.ok.label_font_id = Some(Some(font_id));
            self
        }

        pub fn cancel_label_font_id(mut self, font_id: text::font::Id) -> Self {
            self.style.cancel.label_font_id = Some(Some(font_id));
            self
        }

        pub fn text_box_font_id(mut self, font_id: text::font::Id) -> Self {
            self.style.text_box.font_id = Some(Some(font_id));
            self
        }

        builder_methods! {
            pub ok_label { ok_label = &'a str }
            pub ok_label_color { style.ok.label_color = Some(Color) }
            pub ok_label_font_size { style.ok.label_font_size = Some(FontSize) }
            pub cancel_label { cancel_label = &'a str }
            pub cancel_label_color { style.cancel.label_color = Some(Color) }
            pub cancel_label_font_size { style.cancel.label_font_size = Some(FontSize) }
            pub text_box_color { style.text_box.color = Some(Color) }
            pub text_box_text_color { style.text_box.text_color = Some(Color) }
            pub text_box_border { style.text_box.border = Some(Scalar) }
            pub text_box_border_color { style.text_box.border_color = Some(Color) }
            pub text_box_font_size { style.text_box.font_size = Some(FontSize) }
        }
    }

    widget_ids! {
        pub struct Ids {
            canvas,
            top,
            text,
            bottom,
            left,
            right,
            ok,
            cancel,
        }
    }

    #[derive(Clone, Debug, PartialEq)]
    pub enum Event {
        Update(String),
        Enter,
        Cancel,
    }

    impl From<text_box::Event> for Event {
        fn from(e: text_box::Event) -> Self {
            match e {
                text_box::Event::Update(s) => Event::Update(s),
                text_box::Event::Enter => Event::Enter,
            }
        }
    }

    impl<'a> Widget for TextConfirm<'a> {
        type State = Ids;
        type Style = Style;
        // type Event = Option<Vec<text_box::Event>>;
        type Event = Vec<Event>;

        fn init_state(&self, id_gen: widget::id::Generator) -> Self::State {
            Ids::new(id_gen)
        }

        fn style(&self) -> Self::Style {
            self.style
        }

        fn update(self, args: widget::UpdateArgs<Self>) -> Self::Event {
            let widget::UpdateArgs {
                id,
                state,
                style,
                rect,
                ui,
                ..
            } = args;

            let dim = rect.dim();
            widget::Canvas::new()
                .middle_of(id)
                .graphics_for(id)
                .wh(dim)
                .flow_down(&[
                    (state.top, widget::Canvas::new()),
                    (
                        state.bottom,
                        widget::Canvas::new().flow_right(&[
                            (state.left, widget::Canvas::new()),
                            (state.right, widget::Canvas::new()),
                        ]),
                    ),
                ])
                .set(state.canvas, ui);

            let text_box_color = style.text_box.color(&ui.theme);
            let text_box_text_color = style.text_box.text_color(&ui.theme);
            let text_box_border = style.text_box.border(&ui.theme);
            let text_box_border_color = style.text_box.border_color(&ui.theme);
            let text_box_font_size = style.text_box.font_size(&ui.theme);
            let text_box_font_id = style
                .text_box
                .font_id(&ui.theme)
                .or_else(|| ui.fonts.ids().next());
            let text_box = widget::TextBox::new(self.text_box_text)
                .and_then(text_box_font_id, widget::TextBox::font_id)
                .font_size(text_box_font_size)
                .color(text_box_color)
                .text_color(text_box_text_color)
                .border(text_box_border)
                .border_color(text_box_border_color)
                .middle_of(state.top)
                .wh_of(state.top)
                .set(state.text, ui);

            let ok_label_color = style.ok.label_color(&ui.theme);
            let ok_font_size = style.ok.label_font_size(&ui.theme);
            let ok_font_id = style
                .ok
                .label_font_id(&ui.theme)
                .or_else(|| ui.fonts.ids().next());
            let ok_label_x = style.ok.label_x(&ui.theme);
            let ok_label_y = style.ok.label_y(&ui.theme);
            let ok = widget::Button::new()
                .enabled(true)
                .label(self.ok_label)
                .and_then(ok_font_id, widget::Button::label_font_id)
                .label_color(ok_label_color)
                .label_font_size(ok_font_size)
                .label_x(ok_label_x)
                .label_y(ok_label_y)
                .middle_of(state.left)
                .wh_of(state.left)
                .set(state.ok, ui);

            let cancel_label_color = style.cancel.label_color(&ui.theme);
            let cancel_font_size = style.cancel.label_font_size(&ui.theme);
            let cancel_font_id = style
                .cancel
                .label_font_id(&ui.theme)
                .or_else(|| ui.fonts.ids().next());
            let cancel_label_x = style.cancel.label_x(&ui.theme);
            let cancel_label_y = style.cancel.label_y(&ui.theme);
            let cancel = widget::Button::new()
                .enabled(true)
                .label(self.cancel_label)
                .and_then(cancel_font_id, widget::Button::label_font_id)
                .label_color(cancel_label_color)
                .label_font_size(cancel_font_size)
                .label_x(cancel_label_x)
                .label_y(cancel_label_y)
                .middle_of(state.right)
                .wh_of(state.right)
                .set(state.cancel, ui);

            let events = text_box.into_iter().map(|e| e.into());
            if ok.was_clicked() {
                events
                    // Add an enter event at the end
                    .chain(std::iter::once(Event::Enter))
                    .collect()
            } else if cancel.was_clicked() {
                events
                    // Add a cancel event at the end
                    .chain(std::iter::once(Event::Cancel))
                    .collect()
            } else {
                events.collect()
            }
        }
    }
}
